package com.m3.training.helloworldmsg;
import org.junit.BeforeClass;
import  org.junit.jupiter.api.BeforeAll;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.Test;

class HelloWorldMessageTest {
	
	static private HelloWorldMessage objectUnderTest;

	@BeforeAll
	static void setupBeforeClass() {
		objectUnderTest = 
				new HelloWorldMessage();
	}

	
	@Test
	void test_new_Instance() {
		assertNotNull( objectUnderTest);
		
		
	}
	@Test
	void test_getMessage() {
		assertNotNull( objectUnderTest.getMessage());
	}
		
	
	@Test
	void test_getMessage_twoUniqueMessage() {
		String firstResult = objectUnderTest.getMessage();
		String secondResult = objectUnderTest.getMessage();
		assertNotEquals (firstResult, secondResult);
		
	}

	void doSomethingElse() {
		
	}
}
